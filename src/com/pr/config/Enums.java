package com.pr.config;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class Enums {

	public enum FeatureExteraction {
		AllPixels, PixelsZone4Img28, PixelsZone4Img32;
	}

	public enum Classifiers {
		MPLSingleLayer;
	}

	public enum AllClassifiers {
		NaiveBayes, SVM, MPLSingleLayer;
	}

	public enum Models {
		Training, Validation, Testing;
	}
}
