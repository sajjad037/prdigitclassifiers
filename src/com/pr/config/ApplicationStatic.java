package com.pr.config;

import com.pr.config.Enums.Models;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class ApplicationStatic {
	public static final String[] FOLDERS = { Models.Training.toString(), Models.Validation.toString(),
			Models.Testing.toString() };
	public static final String GENERIC_PATH = "data/";
	public static final String RAW_IMAGES_DIRECTORY = "Persian_Handwritten_digits_Samples";
	public static final String CSV_FEATURES_DIRECTORY = "FeaturesInCSV";
	public static final int[] FOLDER_NUMBERS = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	public static final int IMG_WIDTH = 28, IMG_HEIGHT = 28;
	public static final String SUPPORTED_IMG_FRMT = ".tif";
}
