package com.pr.config;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is the class which use to compete the accuracy and build the confusion
 * matrix of classifier.
 * 
 * @author SajjadAshrafCan
 *
 */
public class AccuracyCalclator {

	double percentCorrect = 0;
	BufferedReader labelFile;
	String[] languages = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
	List<String> languageList = new ArrayList<String>(Arrays.asList(languages));
	int[][] confusionMatrix = new int[10][10];
	int numCorrect = 0;
	int totalSamples = 0;

	/**
	 * Add result to confusion Matrix.
	 * 
	 * @param computedNumber
	 * @param actualNumber
	 */
	public void includeResults(int computedNumber, int actualNumber) {
		confusionMatrix[computedNumber][actualNumber] += 1;

		if (computedNumber == actualNumber) {
			numCorrect++;
		}
		++totalSamples;
	}

	/**
	 * Print the confusion matrix and return the accuracy.
	 * 
	 * @return
	 */
	public double computeAccuracy() {
		System.out.println("totalSamples: " + totalSamples);
		percentCorrect = (numCorrect / (double) totalSamples) * 100;
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("-----------------------------------Confusion Matrix---------------------------------\n");
		stringBuilder.append(String.format("%5s", "") + "\t");
		for (String string : languageList) {
			stringBuilder.append(String.format("%5s", string) + "\t");
		}
		stringBuilder.append("\n");

		for (int i = 0; i < confusionMatrix.length; i++) {
			stringBuilder.append(String.format("%5s", languages[i]) + "\t");

			for (int j = 0; j < confusionMatrix[i].length; j++) {
				stringBuilder.append(String.format("%5s", confusionMatrix[i][j]) + "\t");

			}
			stringBuilder.append("\n");

		}

		System.out.println(stringBuilder);
		return percentCorrect;
	}

}
