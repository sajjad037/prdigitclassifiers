package com.pr.Model;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class DigitImage {

	private int label;
	private double[] data;

	/**
	 * Constructor.
	 * @param label
	 * @param data
	 */
	public DigitImage(int label, double[] data) {
		this.label = label;
		this.data = data;
	}

	/**
	 * get Label or Digit Class
	 * @return
	 */
	public int getLabel() {
		return label;
	}

	/**
	 * Get Data of image in double array.
	 * @return
	 */
	public double[] getData() {
		return data;
	}

}
