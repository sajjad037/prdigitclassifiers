package com.pr.Classifier.NaiveBayes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.pr.config.AccuracyCalclator;

/**
 * 
 * @author Sharanpreet
 *
 */
public class NaiveBayesClassifier {

	private boolean trained = false;
	private double[][] dataLearned;
	int count;
	int classIndex;

	/**
	 * Constructor
	 * 
	 * @param trainingFeatures
	 */
	public NaiveBayesClassifier(List<double[]> trainingFeatures) {

		count = trainingFeatures.get(0).length;
		classIndex = 0;
		dataLearned = new double[10][count];

		dataLearned[0][classIndex] = 1 / (double) 10;
		dataLearned[1][classIndex] = 1 / (double) 10;
		dataLearned[2][classIndex] = 1 / (double) 10;
		dataLearned[3][classIndex] = 1 / (double) 10;
		dataLearned[4][classIndex] = 1 / (double) 10;
		dataLearned[5][classIndex] = 1 / (double) 10;
		dataLearned[6][classIndex] = 1 / (double) 10;
		dataLearned[7][classIndex] = 1 / (double) 10;
		dataLearned[8][classIndex] = 1 / (double) 10;
		dataLearned[9][classIndex] = 1 / (double) 10;

		for (int i = 1; i < count; i++) {

			int oneCount = 0;
			int zeroCount = 0;
			int twoCount = 0;
			int threeCount = 0;
			int fourCount = 0;
			int fiveCount = 0;
			int sixCount = 0;
			int sevenCount = 0;
			int eightCount = 0;
			int nineCount = 0;

			for (double[] j : trainingFeatures) {
				if (j[i] > 0 && j[classIndex] == 1) {
					oneCount++;
				} else if (j[i] > 0 && j[classIndex] == 0) {
					zeroCount++;
				} else if (j[i] > 0 && j[classIndex] == 2) {
					twoCount++;
				} else if (j[i] > 0 && j[classIndex] == 3) {
					threeCount++;
				} else if (j[i] > 0 && j[classIndex] == 4) {
					fourCount++;
				} else if (j[i] > 0 && j[classIndex] == 5) {
					fiveCount++;
				} else if (j[i] > 0 && j[classIndex] == 6) {
					sixCount++;
				} else if (j[i] > 0 && j[classIndex] == 7) {
					sevenCount++;
				} else if (j[i] > 0 && j[classIndex] == 8) {
					eightCount++;
				} else if (j[i] > 0 && j[classIndex] == 9) {
					nineCount++;
				}

			}

			dataLearned[0][i] = (zeroCount + 1) / (double) (600);
			dataLearned[1][i] = (oneCount + 1) / (double) 600;
			dataLearned[2][i] = (twoCount + 1) / (double) (600);
			dataLearned[3][i] = (threeCount + 1) / (double) 600;
			dataLearned[4][i] = (fourCount + 1) / (double) (600);
			dataLearned[5][i] = (fiveCount + 1) / (double) 600;
			dataLearned[6][i] = (sixCount + 1) / (double) (600);
			dataLearned[7][i] = (sevenCount + 1) / (double) 600;
			dataLearned[8][i] = (eightCount + 1) / (double) (600);
			dataLearned[9][i] = (nineCount + 1) / (double) 600;
		}

		trained = true;
	}

	/**
	 * Test the Classifier for CSV Data file
	 * 
	 * @param testingFilPath
	 * @return
	 * @throws Exception
	 */
	public List<double[]> classifyFeatureForFilePath(String testingFilPath) throws Exception {
		if (trained == false) {
			throw new Exception("The classifier must be trained before attempting to classify a feature set.");
		}

		List<double[]> testFeatures = Utility.testfeatureFromFilePath(testingFilPath);

		for (double[] feature : testFeatures) {

			double[] digits = new double[10];

			for (int i = 0; i < digits.length; i++) {
				digits[i] = Math.log(1 / (double) 10);
			}

			for (int i = 1; i < count; i++) {
				for (int j = 0; j < digits.length; j++)
					digits[j] += Math.log((feature[i] > 0) ? dataLearned[j][i] : 1 - dataLearned[j][i]);

			}
			feature[classIndex] = Utility.findMaxArray(digits);
		}
		return testFeatures;
	}

	/**
	 * Test the Classifier for Single row or CSV data file
	 * 
	 * @param inputString
	 * @return
	 * @throws Exception
	 */
	public List<double[]> classifyFeatureForFromSingleInput(String inputString) throws Exception {
		if (trained == false) {
			throw new Exception("The classifier must be trained before attempting to classify a feature set.");
		}

		List<double[]> testFeatures = Utility.testfeatureFromSingleInput(inputString);

		for (double[] feature : testFeatures) {

			double[] digits = new double[10];

			for (int i = 0; i < digits.length; i++) {
				digits[i] = Math.log(1 / (double) 10);
			}

			for (int i = 1; i < count; i++) {
				for (int j = 0; j < digits.length; j++)
					digits[j] += Math.log((feature[i] > 0) ? dataLearned[j][i] : 1 - dataLearned[j][i]);

			}
			feature[classIndex] = Utility.findMaxArray(digits);
		}
		return testFeatures;
	}
	
	/**
	 * calculate Accuracy
	 * @param features
	 * @param testLabelFileName
	 * @return
	 */
	
	public double calculateAccuracy(List<double[]> features, String testLabelFileName) {

		AccuracyCalclator accuracyCalclator = new AccuracyCalclator();
		BufferedReader labelFile;
		try {
			labelFile = new BufferedReader(new FileReader(testLabelFileName));
			String line;
			int index = 0;
			while ((line = labelFile.readLine()) != null && index < features.size()) {
				accuracyCalclator.includeResults((int) features.get(index)[classIndex], Integer.parseInt(line.trim()));
				index++;
			}
		} catch (NumberFormatException | IOException e) { // TODO Auto-generated
															// catch block
			e.printStackTrace();
		}
		return accuracyCalclator.computeAccuracy();

	}
}
