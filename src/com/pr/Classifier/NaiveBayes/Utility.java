package com.pr.Classifier.NaiveBayes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Sharanpreet
 *
 */
public class Utility {

	/**
	 * Train Classifier by passing CSV training data file
	 * @param csvFile
	 * @return
	 */
	public static List<double[]> trainfeature(String csvFile) {
		String line = "";
		String cvsSplitBy = ",";

		List<double[]> list = new ArrayList<double[]>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] feature = line.split(cvsSplitBy);

				double[] intfeatureArray = new double[feature.length];

				for (int i = 0; i < feature.length; i++) {
					intfeatureArray[i] = Double.parseDouble(feature[i]);
				}

				list.add(intfeatureArray);
			}

			System.out.println("Training image count is :: " + list.size());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return list;

	}

	/**
	 * Test the classifier for by passing CSV test data file
	 * @param csvFile
	 * @return
	 */
	public static List<double[]> testfeatureFromFilePath(String csvFile) {

		String line = "";
		String cvsSplitBy = ",";

		List<double[]> list = new ArrayList<double[]>();

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] feature = line.split(cvsSplitBy);

				double[] intfeatureArray = new double[feature.length];

				for (int i = 0; i < feature.length; i++) {
					intfeatureArray[i] = Double.parseDouble(feature[i]);
				}

				list.add(intfeatureArray);

			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * Test the classifier for by passing single row of CSV testing file.
	 * @param inputString
	 * @return
	 */
	public static List<double[]> testfeatureFromSingleInput(String inputString) {

		String cvsSplitBy = ",";

		List<double[]> list = new ArrayList<double[]>();

		try {
			// use comma as separator
			String[] feature = inputString.split(cvsSplitBy);

			double[] intfeatureArray = new double[feature.length];

			for (int i = 0; i < feature.length; i++) {
				intfeatureArray[i] = Double.parseDouble(feature[i]);
			}

			list.add(intfeatureArray);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;

	}

	public static int findMaxArray(double[] array) {

		int maxIndex = 0;
		for (int i = 1; i < array.length; i++) {
			double newnumber = array[i];
			if ((newnumber > array[maxIndex])) {
				maxIndex = i;
			}
		}

		return maxIndex;
	}

}
