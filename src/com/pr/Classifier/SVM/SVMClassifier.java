package com.pr.Classifier.SVM;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.pr.Model.DigitImage;
import com.pr.config.AccuracyCalclator;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;
import libsvm.svm_problem;

/**
 * 
 * @author Farhan, Mansoor
 *
 */
public class SVMClassifier {

	/**
	 * Classify the testing data, by first training the classifier with training
	 * data.
	 * 
	 * @param trainingFilPath
	 * @param testingFilPath
	 * @throws Exception
	 */
	public void Classify(String trainingFilPath, String testingFilPath) throws Exception {
		List<DigitImage> dataTrain = readFile(trainingFilPath);
		List<DigitImage> datatest = readFile(testingFilPath);

		svm_model m = svmTrain(dataTrain);

		double[] ypred = svmPredict(datatest, m);

		AccuracyCalclator accuracyCalclator = new AccuracyCalclator();
		DecimalFormat df2 = new DecimalFormat("###.##");
		for (int i = 0; i < datatest.size(); i++) {
			accuracyCalclator.includeResults(datatest.get(i).getLabel(), (int) ypred[i]);
		}
		double percentAccuracy = accuracyCalclator.computeAccuracy();
		System.out.println("Accuracy: " + df2.format(percentAccuracy) + "%");

	}

	/**
	 * Train SVM
	 * 
	 * @param dataTrain
	 * @return
	 */
	public svm_model svmTrain(List<DigitImage> dataTrain) {
		svm_problem prob = new svm_problem();
		int recordCount = dataTrain.size();
		int featureCount = dataTrain.get(0).getData().length;
		prob.y = new double[recordCount];
		prob.l = recordCount;
		prob.x = new svm_node[recordCount][featureCount];

		for (int i = 0; i < recordCount; i++) {
			double[] features = dataTrain.get(i).getData();

			prob.x[i] = new svm_node[features.length];
			for (int j = 0; j < features.length; j++) {
				svm_node node = new svm_node();
				node.index = j;
				node.value = features[j];
				prob.x[i][j] = node;
			}
			prob.y[i] = dataTrain.get(i).getLabel();
		}

		svm_parameter param = new svm_parameter();
		param.probability = 1;
		param.gamma = 0.5;
		param.nu = 0.5;
		param.C = 100;
		param.svm_type = svm_parameter.C_SVC;
		param.kernel_type = svm_parameter.LINEAR;
		param.cache_size = 20000;
		param.eps = 0.001;

		svm_model model = svm.svm_train(prob, param);

		return model;
	}

	/**
	 * Predict the class of digits by providing the CSV test file data.
	 * 
	 * @param dataTest
	 * @param model
	 * @return
	 */
	public double[] svmPredict(List<DigitImage> dataTest, svm_model model) {

		double[] yPred = new double[dataTest.size()];

		for (int k = 0; k < dataTest.size(); k++) {

			double[] fVector = dataTest.get(k).getData();

			svm_node[] nodes = new svm_node[fVector.length];
			for (int i = 0; i < fVector.length; i++) {
				svm_node node = new svm_node();
				node.index = i;
				node.value = fVector[i];
				nodes[i] = node;
			}

			int totalClasses = 10;
			int[] labels = new int[totalClasses];
			svm.svm_get_labels(model, labels);

			double[] prob_estimates = new double[totalClasses];
			yPred[k] = svm.svm_predict_probability(model, nodes, prob_estimates);

		}

		return yPred;
	}

	@SuppressWarnings("resource")
	/**
	 * Read CSV file and transform the cSV file data to List<DigitImage>
	 * Dataset.
	 * 
	 * @param labelFileName
	 * @return
	 * @throws Exception
	 */
	private List<DigitImage> readFile(String labelFileName) throws Exception {
		List<DigitImage> images = new ArrayList<DigitImage>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		br = new BufferedReader(new FileReader(labelFileName));
		while ((line = br.readLine()) != null) {

			// use comma as separator
			String[] values = line.split(cvsSplitBy);
			double[] nums = new double[values.length - 1];

			int lable = Integer.parseInt(values[0]);
			for (int i = 0; i < nums.length; i++) {
				nums[i] = Double.parseDouble(values[i + 1]);
			}

			images.add(new DigitImage(lable, nums));
		}

		return images;
	}
}
