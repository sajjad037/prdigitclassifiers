package com.pr.Classifier.NeuralNetwork;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pr.config.AccuracyCalclator;

/**
 * This Code build with the help of https://deeplearning4j.org/ A Simple Multi
 * Layered Perceptron (MLP) applied to digit classification for the Persian
 * Handwritten digits Samples.
 *
 * This file builds one input layer and one hidden layer.
 *
 * The input layer has input dimension of numRows*numColumns where these
 * variables indicate the number of vertical and horizontal pixels in the image.
 * This layer uses a rectified linear unit (relu) activation function. The
 * weights for this layer are initialized by using Xavier initialization
 * (https://prateekvjoshi.com/2016/03/29/understanding-xavier-initialization-in-deep-neural-networks/)
 * to avoid having a steep learning curve. This layer will have 1000 output
 * signals to the hidden layer.
 *
 * The hidden layer has input dimensions of 1000. These are fed from the input
 * layer. The weights for this layer is also initialized using Xavier
 * initialization. The activation function for this layer is a softmax, which
 * normalizes all the 10 outputs such that the normalized sums add up to 1. The
 * highest of these normalized values is picked as the predicted class.
 *
 */

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class MPLSingleLayerClassifier implements BaseClassifier {

	private static Logger log = LoggerFactory.getLogger(MPLSingleLayerClassifier.class);

	@Override
	public void Classify(String filenameTrain, String filenameTest, int outputNum, int batchSize, int numEpochs,
			int rngSeed, int numRows, int numColumns) throws Exception {

		// Train
		MultiLayerNetwork model = train(filenameTrain, outputNum, batchSize, numEpochs, rngSeed, numRows, numColumns);

		// Load the test/evaluation data:
		predictDataSet(filenameTest, outputNum, model);

	}

	/**
	 * Training the MPL
	 * 
	 * @param filenameTrain
	 * @param outputNum
	 * @param batchSize
	 * @param numEpochs
	 * @param rngSeed
	 * @param numRows
	 * @param numColumns
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public MultiLayerNetwork train(String filenameTrain, int outputNum, int batchSize, int numEpochs, int rngSeed,
			int numRows, int numColumns) throws IOException, InterruptedException {

		System.out.println("Running MPLSingleLayerClassifier.......");

		Path p = Paths.get(filenameTrain);
		String trainedFile = p.getFileName().toString().replace(".csv", "_SMPL.zip");

		MultiLayerNetwork model = null;
		File trainedFileSaveLoc = new File(trainedFile);

		// Check Trained File exist
		if (trainedFileSaveLoc.exists()) {
			model = ModelSerializer.restoreMultiLayerNetwork(trainedFileSaveLoc);
		} else {
			// Load the training data:
			RecordReader rr = new CSVRecordReader();
			rr.initialize(new FileSplit(new File(filenameTrain)));
			DataSetIterator mnistTrain = new RecordReaderDataSetIterator(rr, batchSize, 0, 10);

			log.info("Build model....");
			MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(rngSeed)
					// include a random seed for reproducibility
					// use stochastic gradient descent as an optimization
					// algorithm
					.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
					.learningRate(0.006)
					// specify the learning rate
					.updater(Updater.NESTEROVS).momentum(0.9)
					// specify the rate of change of the learning rate.
					.regularization(true).l2(1e-4).list()
					.layer(0,
							new DenseLayer.Builder()
									// create the first, input layer with xavier
									// initialization
									.nIn(numRows * numColumns).nOut(1000).activation(Activation.RELU)
									.weightInit(WeightInit.XAVIER).build())
					.layer(1,
							new OutputLayer.Builder(LossFunction.NEGATIVELOGLIKELIHOOD)
									// create hidden layer
									.nIn(1000).nOut(outputNum).activation(Activation.SOFTMAX)
									.weightInit(WeightInit.XAVIER).build())
					.pretrain(false).backprop(true)
					// use backpropagation to adjust weights
					.build();

			model = new MultiLayerNetwork(conf);
			model.init();
			// print the score with every 1 iteration
			model.setListeners(new ScoreIterationListener(1));

			log.info("Train model....");
			for (int i = 0; i < numEpochs; i++) {
				model.fit(mnistTrain);
			}

			log.info("Save the Trained model....");
			boolean saveUpdate = false;
			ModelSerializer.writeModel(model, trainedFileSaveLoc, saveUpdate);

		}

		return model;
	}

	/**
	 * Test the classifier for CSV test data file.
	 * 
	 * @param filenameTest
	 * @param outputNum
	 * @param model
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private void predictDataSet(String filenameTest, int outputNum, MultiLayerNetwork model)
			throws IOException, InterruptedException {
		// Read CSV test data file.
		RecordReader rrTest = new CSVRecordReader();
		rrTest.initialize(new FileSplit(new File(filenameTest)));
		DataSetIterator mnistTest = new RecordReaderDataSetIterator(rrTest, 2000, 0, 10);

		// now predict the class of digits.
		DataSet datasetTest = mnistTest.next();
		INDArray features = datasetTest.getFeatureMatrix();
		int[] prediction = model.predict(features);
		AccuracyCalclator accuracyCalclator = new AccuracyCalclator();
		DecimalFormat df2 = new DecimalFormat("###.##");

		for (int i = 0; i < features.rows(); i++) {
			accuracyCalclator.includeResults(Nd4j.getBlasWrapper().iamax(datasetTest.getLabels().getRow(i)),
					prediction[i]);
		}
		double percentAccuracy = accuracyCalclator.computeAccuracy();
		System.out.println("Accuracy: " + df2.format(percentAccuracy) + "%");

		log.info("****************Example finished********************");
	}

	/**
	 * Predict the Class of digit by providing a single line of CSV test file .
	 * 
	 * @param datasetTest
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public int[] predict(DataSet datasetTest, MultiLayerNetwork model) throws IOException, InterruptedException {
		INDArray features = datasetTest.getFeatureMatrix();
		return model.predict(features);
	}

}
