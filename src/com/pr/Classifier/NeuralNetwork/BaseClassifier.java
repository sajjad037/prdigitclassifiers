package com.pr.Classifier.NeuralNetwork;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public interface BaseClassifier {

	/**
	 * Classify the testing data, by first training the classifier with training data.
	 * @param filenameTrain
	 * @param filenameTest
	 * @param outputNum
	 * @param batchSize
	 * @param numEpochs
	 * @param rngSeed
	 * @param numRows
	 * @param numColumns
	 * @throws Exception
	 */
	void Classify(String filenameTrain, String filenameTest, int outputNum, int batchSize, int numEpochs, int rngSeed,
			int numRows, int numColumns) throws Exception;
}
