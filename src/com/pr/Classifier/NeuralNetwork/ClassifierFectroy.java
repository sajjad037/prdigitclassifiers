package com.pr.Classifier.NeuralNetwork;

import com.pr.config.Enums.Classifiers;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class ClassifierFectroy {
	/**
	 * Get the Classifiers instance
	 * @param classifiers
	 * @return
	 */
	public BaseClassifier getClassifier(Classifiers classifiers) {
		switch (classifiers) {
		case MPLSingleLayer:
			return new MPLSingleLayerClassifier();

		default:
			return null;
		}
	}
}
