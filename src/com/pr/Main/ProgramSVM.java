package com.pr.Main;

import com.pr.Classifier.SVM.SVMClassifier;;

public class ProgramSVM {

	static String trainingFilePath = "";
	static String testingFilePath = "";

	/**
	 * SVM Main program Execution. 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths.
		 */

		/**
		 * SVMClassifier for AllPixels when Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		trainingFilePath = "data/FeaturesInCSV/Training/AllPixels.csv";
		testingFilePath = "data/FeaturesInCSV/Validation/AllPixels.csv";


		/**
		 * SVMClassifier for Zoning of the image by 4*4 matrix on 28 * 28 image matrix:
		 * Strategy.
		 */
		trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img28.csv";
		testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img28.csv";

		/**
		 * SVMClassifier for Zoning of the image by 4*4 matrix on 32 * 32 image matrix
		 * Extraction Strategy.
		 */
		 trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img32.csv";
		 testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img32.csv";
		
		System.out.print("Training File Path :" + trainingFilePath + "\r\n");
		System.out.print("Testing File Path :" + testingFilePath + "\r\n");
		SVMClassifier svmClassifier = new SVMClassifier();
		svmClassifier.Classify(trainingFilePath, testingFilePath);
	}
}
