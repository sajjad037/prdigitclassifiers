package com.pr.Main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.datavec.api.records.reader.RecordReader;
import org.datavec.api.records.reader.impl.csv.CSVRecordReader;
import org.datavec.api.split.FileSplit;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import com.pr.Classifier.NaiveBayes.NaiveBayesClassifier;
import com.pr.Classifier.NaiveBayes.Utility;
import com.pr.Classifier.NeuralNetwork.MPLSingleLayerClassifier;
import com.pr.Classifier.SVM.SVMClassifier;
import com.pr.Model.DigitImage;
import com.pr.config.AccuracyCalclator;
import com.pr.config.Enums.AllClassifiers;
import com.pr.config.Enums.FeatureExteraction;
import com.pr.config.Enums.Models;

import libsvm.svm_model;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class ProgramPatternRecognitionSystem {

	static String genericTraningPath = "data/FeaturesInCSV/%s/%s.csv";

	// Declare All three Classifier
	static NaiveBayesClassifier naiveBayesClassifier = null;
	static SVMClassifier svmClassifier = null;
	static MPLSingleLayerClassifier singleLayerNNClassifier = null;

	// DataSets for All three Classifier
	static List<DigitImage> testDataSet = null;
	static String naivetestDataSet[] = null;

	// Extra
	static int rngseed = 123;
	static int batchSize = 128;
	static int outputNum = 10;
	static int numEpochs = 100;
	static int rngSeed = 123;
	static int numRows = 2;
	static int numColumns = 28;

	/**
	 * Main Program Execution
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {

		/**
		 * ************************* Training ******************************
		 * Train all three classifiers with respective Features techniques on
		 * which Classifier performed best.
		 */

		// Naive Bayes
		String path = getFilePath(AllClassifiers.NaiveBayes, Models.Training, FeatureExteraction.AllPixels);
		List<double[]> trainingFeatures = Utility.trainfeature(path);
		naiveBayesClassifier = new NaiveBayesClassifier(trainingFeatures);

		// SVM
		svmClassifier = new SVMClassifier();
		path = getFilePath(AllClassifiers.SVM, Models.Training, FeatureExteraction.AllPixels);
		List<DigitImage> svmTrainDataSet = readFile(path, AllClassifiers.SVM);
		svm_model svmModel = svmClassifier.svmTrain(svmTrainDataSet);

		// SingleLayerNN
		singleLayerNNClassifier = new MPLSingleLayerClassifier();
		path = getFilePath(AllClassifiers.MPLSingleLayer, Models.Training, FeatureExteraction.AllPixels);
		numColumns = 28;
		numRows = 28;
		MultiLayerNetwork model = singleLayerNNClassifier.train(path, outputNum, batchSize, numEpochs, rngSeed, numRows,
				numColumns);

		/**
		 * ************************* Testing ***********************************
		 * Now test the testing and validation data on trained classifiers.
		 * Note: for test data provide the testing file of respective feature to
		 * the classifier.
		 */

		// *********************** Get Testing Data ****************************

		// SVM
		Models modelDataset = Models.Validation;
		path = getFilePath(AllClassifiers.SVM, modelDataset, FeatureExteraction.AllPixels);
		testDataSet = readFile(path, AllClassifiers.SVM);

		// Naive Bayes
		path = getFilePath(AllClassifiers.NaiveBayes, modelDataset, FeatureExteraction.AllPixels);
		naivetestDataSet = readFromFile(path, Charset.defaultCharset()).toLowerCase().replaceAll("\\s*\n\\s*", "\n")
				.split("\n");

		// Multi Layer NN
		path = getFilePath(AllClassifiers.MPLSingleLayer, modelDataset, FeatureExteraction.AllPixels);
		RecordReader rrTest = new CSVRecordReader();
		rrTest.initialize(new FileSplit(new File(path)));
		DataSetIterator mnistTest = new RecordReaderDataSetIterator(rrTest, 1, 0, 10);

		int size = testDataSet.size();
		AccuracyCalclator accuracyCalclator = new AccuracyCalclator();
		int actualValue = 0;
		int naiveComputedValue = 0, svmComputedValue = 0, nnComputedValue = 0, computedValue = 0;

		for (int s = 0; s < size; s++) {

			actualValue = testDataSet.get(s).getLabel();

			// ******************** Perform Testing ****************************

			// Naive bayes
			naiveComputedValue = getNaivePrediction(s);

			// SVM
			List<DigitImage> dataTest = new ArrayList<DigitImage>();
			dataTest.add(testDataSet.get(s));
			double svmVal[] = svmClassifier.svmPredict(dataTest, svmModel);
			svmComputedValue = (int) svmVal[0];

			// singleLayerNNClassifier
			DataSet datasetTest = mnistTest.next();
			int val[] = singleLayerNNClassifier.predict(datasetTest, model);
			nnComputedValue = (int) val[0];

			/**
			 * ********************* Post processing *************************
			 * Compare the out put of all three classifier and select the out
			 * put which have more votes, in In case all classifier has
			 * different out puts. Then select the result of master classifier.
			 */
			computedValue = postProcessing(nnComputedValue, svmComputedValue, naiveComputedValue);
        	System.out.println(String.format("Acutal: %d, NN: %d, SVM: %d, Navie: %d", actualValue, naiveComputedValue, svmComputedValue, nnComputedValue));

			// Build the confusion matrix
			accuracyCalclator.includeResults(actualValue, computedValue);
		}

		// Print the Accuracy.
		double acc = accuracyCalclator.computeAccuracy();
		System.out.println("Accuracy with "+modelDataset.toString()+" data set is " + acc + "%.");

	}

	/**
	 * get Naive Prediction
	 * 
	 * @param index
	 * @return
	 * @throws Exception
	 */
	private static int getNaivePrediction(int index) throws Exception {
		// Data
		String data = naivetestDataSet[index];

		// Testing Naive Bayes
		List<double[]> testingFeaturesResult = naiveBayesClassifier.classifyFeatureForFromSingleInput(data);
		int classIndex =0;
		return (int) testingFeaturesResult.get(0)[classIndex];
	}

	/**
	 * Get File Path
	 * 
	 * @param classifier
	 * @param model
	 * @param featureTechnique
	 * @return
	 */
	private static String getFilePath(AllClassifiers classifier, Models model, FeatureExteraction featureTechnique) {

		switch (classifier) {
		case NaiveBayes:
			return String.format(genericTraningPath, model.toString(), featureTechnique.toString());

		case SVM:
			return String.format(genericTraningPath, model.toString(), featureTechnique.toString());

		case MPLSingleLayer:

			return String.format(genericTraningPath, model.toString(), featureTechnique.toString());

		default:
			break;
		}
		return null;
	}

	/**
	 * read From File
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 */
	private static String readFromFile(String path, Charset encoding) {
		try {
			if (new File(path).exists()) {
				byte[] encoded = Files.readAllBytes(Paths.get(path));
				return new String(encoded, encoding);
			} else {
				return "";
			}

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * Post Processing
	 * 
	 * @param primarayClassifierValue
	 * @param secondClassifierValue
	 * @param thirdClassifierValue
	 * @return
	 */
	private static int postProcessing(int primarayClassifierValue, int secondClassifierValue,
			int thirdClassifierValue) {

		int finalValue = -1;

		if (primarayClassifierValue == secondClassifierValue && primarayClassifierValue == thirdClassifierValue) {
			finalValue = primarayClassifierValue;
		} else if (primarayClassifierValue == secondClassifierValue
				|| primarayClassifierValue == thirdClassifierValue) {
			finalValue = primarayClassifierValue;
		} else if (secondClassifierValue == thirdClassifierValue) {
			finalValue = secondClassifierValue;
		} else {
			finalValue = primarayClassifierValue;
		}
		return finalValue;
	}

	@SuppressWarnings("resource")
	/**
	 * read File CSV file and build List<DigitImage> DataSet.
	 * 
	 * @param labelFileName
	 * @param classifier
	 * @return
	 * @throws Exception
	 */
	private static List<DigitImage> readFile(String labelFileName, AllClassifiers classifier) throws Exception {
		List<DigitImage> images = new ArrayList<DigitImage>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		br = new BufferedReader(new FileReader(labelFileName));
		while ((line = br.readLine()) != null) {

			// use comma as separator
			String[] values = line.split(cvsSplitBy);
			double[] nums = new double[values.length - 1];
			int lable = 0;
			
			lable = Integer.parseInt(values[0]);
			
			for (int i = 0; i < nums.length; i++) {
				nums[i] = Double.parseDouble(values[i + 1]);
			}

			images.add(new DigitImage(lable, nums));
		}

		return images;
	}
}
