package com.pr.Main;

import com.pr.Classifier.NeuralNetwork.BaseClassifier;
import com.pr.Classifier.NeuralNetwork.ClassifierFectroy;
import com.pr.config.Enums.Classifiers;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class ProgramMPLClassifiers {

	private static String trainingFilePath = "";
	private static String testingFilePath = "";

	/**
	 * Path (dirTrain, dirTest) of Directory Which contains actual Images but in
	 * PNG format. and line # 76 replace with. Classifiers classifiersAlgo =
	 * Classifiers.MPLImagePipeline; This Classifiers directly read images and
	 * trained the neural network.
	 */
	private static int batchSize = 128;
	private static int outputNum = 10;
	private static int numEpochs = 100;
	private static int rngSeed = 123;
	private static int numRows = 2;
	private static int numColumns = 28;

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths. If you want to train the network from start delete all
		 * .zip file from Project directory like 'AllPixels_16_16_SMPL.zip',
		 * 'PixelsZone4IMG16_SMPL.zip' etc...
		 */

		/**
		 * MultipleLayerPerceptron for AllPixels when Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		//numColumns = 28;
		//numRows = 28;
		//trainingFilePath = "data/FeaturesInCSV/Training/AllPixels.csv";
		//testingFilePath = "data/FeaturesInCSV/Validation/AllPixels.csv";

		

		/**
		 * MultipleLayerPerceptron for Zoning of the image by 4*4 matrix on 28 * 28 image matrix:
		 * Strategy.
		 */
		numColumns = 7;
		numRows = 7;
		trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img28.csv";
		testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img28.csv";

		/**
		 * MultipleLayerPerceptron for Zoning of the image by 4*4 matrix on 32 * 32 image matrix
		 * Extraction Strategy.
		 */
		 //numColumns = 8;
		 //numRows = 8;
		 //trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img32.csv";
		 //testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img32.csv";
		 
		Classifiers classifiersAlgo = Classifiers.MPLSingleLayer;
		BaseClassifier baseClassifier = new ClassifierFectroy().getClassifier(classifiersAlgo);
		System.out.print("Training File Path :" + trainingFilePath + "\r\n");
		System.out.print("Testing File Path :" + testingFilePath + "\r\n");
		baseClassifier.Classify(trainingFilePath, testingFilePath, outputNum, batchSize, numEpochs, rngSeed, numRows,
				numColumns);

	}
}
