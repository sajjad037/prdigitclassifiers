package com.pr.Main;

import java.util.List;

import com.pr.Classifier.NaiveBayes.NaiveBayesClassifier;
import com.pr.Classifier.NaiveBayes.Utility;

public class ProgramNaiveBayes {

	private static String trainingFilePath = "";
	private static String testingFilePath = "";

	public static void main(String[] args) throws Exception {

		/**
		 * To Test Specific Extraction Technique Kindly uncomment the respective
		 * file paths.
		 */

		/**
		 * NaiveBayes for AllPixels when Image Size 28*28 Feature Extraction
		 * Strategy.
		 */
		trainingFilePath = "data/FeaturesInCSV/Training/AllPixels.csv";
		testingFilePath = "data/FeaturesInCSV/Validation/AllPixels.csv";

		/**
		 * NaiveBayes for Zoning of the image by 4*4 matrix on 28 * 28 image matrix:
		 * Strategy.
		 */
		trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img28.csv";
		testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img28.csv";

		/**
		 * NaiveBayes for Zoning of the image by 4*4 matrix on 32 * 32 image matrix
		 * Extraction Strategy.
		 */
		 //trainingFilePath = "data/FeaturesInCSV/Training/PixelsZone4Img32.csv";
		 //testingFilePath = "data/FeaturesInCSV/Validation/PixelsZone4Img32.csv";

		System.out.print("Training File Path :" + trainingFilePath + "\r\n");
		System.out.print("Testing File Path :" + testingFilePath + "\r\n");
		List<double[]> trainingFeatures = Utility.trainfeature(trainingFilePath);
		NaiveBayesClassifier classifier = new NaiveBayesClassifier(trainingFeatures);

		List<double[]> testingFeaturesResult = classifier.classifyFeatureForFilePath(testingFilePath);
		double accuracyTestingLabel = classifier.calculateAccuracy(testingFeaturesResult, "testlabels.txt");

		System.out.println("Accuracy with testing data set is " + accuracyTestingLabel + "%.");

	}

}
