package com.pr.Main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.pr.FeatureExtraction.FeaturesFectory;
import com.pr.FeatureExtraction.IFeatureExtraction;
import com.pr.IOFiling.SaveFile;
import com.pr.Model.DigitImage;
import com.pr.config.ApplicationStatic;
import com.pr.config.Enums.FeatureExteraction;

/**
 * 
 * @author SajjadAshrafCan
 *
 */
public class ProgramGenerateCSVFeatureVectore {

	public static void main(String[] args) throws IOException {
		/**
		 * First goto com.ai.config.ApplicationStatic.java file and set or
		 * verify the values of following variables.
		 * 
		 * FOLDERS : set folder name you have like Testing, Training and
		 * Validation.
		 * 
		 * GENERIC_PATH : set generic path to reach these folders.
		 * 
		 * FOLDER_NUMBERS : set number that you have inside of Testing, Training
		 * and Validation.
		 * 
		 * IMG_WIDTH : Set the image width.
		 * 
		 * IMG_HEIGHT : set the images height.
		 * 
		 * SUPPORTED_IMG_FRMT : what type of format your images have.
		 */

		/**
		 * To Generate All Type Features Extraction in once, uncomment following
		 * code.
		 */
		GenerateAllFeatureExtraction();

		/**
		 * To Generate Specific Features Extraction pass a respective
		 * FeatureExteraction type, execute following code.
		 */
		// GenerateAllFeatureExtraction(FeatureExteraction.AllPixels);
		// GenerateAllFeatureExtraction(FeatureExteraction.AllPixelsImg16);

		/**
		 * other examples.
		 * 
		 */
		// GenerateAllFeatureExtraction(FeatureExteraction.PixelsZone4IMG16);
		// GenerateAllFeatureExtraction(FeatureExteraction.PixelsZone4IMG32);
		// GenerateAllFeatureExtraction(FeatureExteraction.SUMOfPixels);
	}

	/**
	 * Generate CSV files for All available Feature Extraction
	 * @throws IOException
	 */
	private static void GenerateAllFeatureExtraction() throws IOException {
		long startingTime = Calendar.getInstance().getTimeInMillis();

		for (FeatureExteraction extertaction : FeatureExteraction.values()) {
			GenerateForOneFeatureExtraction(extertaction);
		}

		long EndingTime = Calendar.getInstance().getTimeInMillis();
		System.out.println(String.format("Total Time: %d (MILLISECOND)", (EndingTime - startingTime)));
	}

	/**
	 * Generate CSV files for input Feature Extraction
	 * @param extertaction
	 * @throws IOException
	 */
	private static void GenerateForOneFeatureExtraction(FeatureExteraction extertaction) throws IOException {

		long startingTime = Calendar.getInstance().getTimeInMillis();
		String rawImgsPath = "";
		String csvFeatureFilePath = "";
		SaveFile saveFile = new SaveFile();

		IFeatureExtraction featureExtraction = new FeaturesFectory().getFeatureExtraction(extertaction);
		int folderCout = ApplicationStatic.FOLDERS.length;

		for (int f = 0; f < folderCout; f++) {
			String folder = ApplicationStatic.FOLDERS[f];

			// Set the Sub folder name {"Training", "Validation", "Testing"}
			rawImgsPath = String.format(ApplicationStatic.GENERIC_PATH + "%s/%s/",
					ApplicationStatic.RAW_IMAGES_DIRECTORY, folder);

			// Set path to save features CSV files
			csvFeatureFilePath = String.format(ApplicationStatic.GENERIC_PATH + "%s/%s/",
					ApplicationStatic.CSV_FEATURES_DIRECTORY, folder);

			List<DigitImage> images = new ArrayList<DigitImage>();

			int numberCount = ApplicationStatic.FOLDER_NUMBERS.length;
			for (int i = 0; i < numberCount; i++) {
				int currentNum = ApplicationStatic.FOLDER_NUMBERS[i];
				String path = String.format(rawImgsPath + "%d", currentNum);
				File[] files = new File(path).listFiles();

				for (File file : files) {
					if (file.isFile() && file.getName().toLowerCase().endsWith(ApplicationStatic.SUPPORTED_IMG_FRMT)) {
						images.add(featureExtraction.Extract(file, currentNum));
						System.out.println("FileName : " + file.getName());
					}
				}
				System.out.println("****** Folder " + i + " Path : " + path + " *********");
			}

			saveFile.saveInCSV(images, csvFeatureFilePath + extertaction.toString() + ".csv");
			System.out.println("images Size (folders) : " + images.size());
		}

		long EndingTime = Calendar.getInstance().getTimeInMillis();
		System.out.println(String.format("Total Time: %d (MILLISECOND)", (EndingTime - startingTime)));
	}
}