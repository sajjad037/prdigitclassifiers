package com.pr.FeatureExtraction;

import java.io.File;

import com.pr.IOFiling.ReadImage;
import com.pr.Model.DigitImage;

/**
 * 
 * @author Farhan
 *
 */
public class ExtractPixelsZone4Img32 implements IFeatureExtraction {

	@Override
	public DigitImage Extract(File file, int lable) {
		// Normalize the Image Matrix
		ReadImage readImg = new ReadImage();
		double[][] imgMatrix;
		imgMatrix = readImg.getNormalizeMatrix(file, 32, 32);
		
		return new ExtractPixelsZone4().Extract(imgMatrix, lable, 4, 1);
	}
}
