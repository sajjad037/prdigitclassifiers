package com.pr.FeatureExtraction;

import java.text.DecimalFormat;
import java.util.Arrays;

import com.pr.config.Enums.FeatureExteraction;

/**
 * 
 * @author Farhan
 *
 */
public class FeaturesFectory {
	static DecimalFormat df = new DecimalFormat("#.##");

	/**
	 * Get Feature Extraction instance
	 * @param featureExteraction
	 * @return
	 */
	public IFeatureExtraction getFeatureExtraction(FeatureExteraction featureExteraction) {
		switch (featureExteraction) {
		case AllPixels:
			return new ExtractAllPixels();

		case PixelsZone4Img28:
			return new ExtractPixelsZone4();

		case PixelsZone4Img32:
			return new ExtractPixelsZone4Img32();

		default:
			return null;
		}
	}

	private static double Normalize(double value, double min, double max) {
		// Normalize the value (0-1).
		// X = (X - min) / (max - min)
		return Double.valueOf(df.format((value - min) / (max - min)));
	}

	/**
	 * Normalize double into 1 and 0's
	 * 
	 * @param inputValues
	 * @return
	 */
	public static double[] Normalize(double[] inputValues) {
		double min = minValue(inputValues);
		double max = maxValue(inputValues);
		double[] values = new double[inputValues.length];
		for (int i = 0; i < inputValues.length; i++) {
			values[i] = Normalize(inputValues[i], min, max);
		}
		return values;
	}

	private static double maxValue(double array[]) {
		return Arrays.stream(array).max().getAsDouble();
	}

	private static double minValue(double array[]) {
		return Arrays.stream(array).min().getAsDouble();
	}
}
