package com.pr.FeatureExtraction;

import java.io.File;

import com.pr.Model.DigitImage;

/**
 * 
 * @author Farhan
 *
 */
public interface IFeatureExtraction {
	/**
	 * Extract features. 
	 * @param file
	 * @param lable
	 * @return
	 */
	DigitImage Extract(File file, int lable);	
}
