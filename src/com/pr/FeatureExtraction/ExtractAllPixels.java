package com.pr.FeatureExtraction;

import java.io.File;

import com.pr.IOFiling.ReadImage;
import com.pr.Model.DigitImage;

/**
 * 
 * @author Farhan
 *
 */
public class ExtractAllPixels implements IFeatureExtraction {

	@Override
	public DigitImage Extract(File file, int lable) {

		// getNormalize Image Matrix
		ReadImage readImg = new ReadImage();
		double[][] imgMatrix;
		imgMatrix = readImg.getNormalizeMatrix(file);

		return Extract(imgMatrix, lable);
	}

	/**
	 * Extract Features
	 * @param imgMatrix
	 * @param lable
	 * @return
	 */
	public DigitImage Extract(double[][] imgMatrix, int lable) {
		// convert all row to column
		int rows = imgMatrix.length;
		int cols = imgMatrix[0].length;
		int featureLength = rows * cols;
		double[] feature = new double[featureLength];

		int index = 0;
		// Sum of column values
		for (int col = 0; col < cols; col++) {
			for (int row = 0; row < rows; row++) {
				// index = col + row;
				// feature[index] = imgMatrix[col][row];
				feature[index] = imgMatrix[col][row];
				++index;
			}
		}

		// Here not need to normalize feature vector values already in 0 and 1
		// form.
		// feature = FeaturesFectory.Normalize(feature);
		return new DigitImage(lable, feature);
	}

}
