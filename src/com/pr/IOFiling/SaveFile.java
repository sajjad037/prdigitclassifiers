package com.pr.IOFiling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

import com.pr.Model.DigitImage;
import com.pr.config.Enums.FeatureExteraction;

/**
 * 
 * @author Mansoor
 *
 */
public class SaveFile {
	
	/**
	 * Save the List<DigitImage> data into CSV file
	 * @param data
	 * @param filePath
	 * @throws FileNotFoundException
	 */
	public void saveInCSV(List<DigitImage> data, String filePath) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(new File(filePath));
		StringBuilder sb = new StringBuilder();
		int count = data.size();
			
		if(filePath.contains(FeatureExteraction.AllPixels.toString()))
		{

			for (int i = 0; i < count; i++) {
				int lable = data.get(i).getLabel();
				String commaSeperatedData = convertToCommaSeparated(data.get(i).getData());				
				commaSeperatedData = commaSeperatedData.replaceAll("1.0", "s").replaceAll("0.0", "1.0").replaceAll("s", "0.0");
				sb.append(lable + "," + commaSeperatedData);
				sb.append('\n');
			}
			pw.write(sb.toString());
			pw.close();
			
		}
		else
		{
			for (int i = 0; i < count; i++) {
				int lable = data.get(i).getLabel();
				String commaSeperatedData = convertToCommaSeparated(data.get(i).getData());
				sb.append(lable + "," + commaSeperatedData);
				sb.append('\n');
			}
			pw.write(sb.toString());
			pw.close();
		}	
		
	}

	/**
	 * Convert double array into comma separated string.
	 * @param data
	 * @return
	 */
	private static String convertToCommaSeparated(double[] data) {
		String value = "";
		for (double d : data) {
			value += d + ",";
		}
		value = value.trim().substring(0, value.length() - 1);
		return value;
	}
}
