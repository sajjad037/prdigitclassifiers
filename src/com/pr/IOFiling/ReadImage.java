package com.pr.IOFiling;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;

import javax.imageio.ImageIO;

import com.pr.config.ApplicationStatic;

/**
 * 
 * @author Mansoor
 *
 */
public class ReadImage {

	/**
	 * Get Normalize Matrix
	 * 
	 * @param file
	 * @return
	 */
	public double[][] getNormalizeMatrix(File file) {
		return getNormalizeMatrix(file, ApplicationStatic.IMG_WIDTH, ApplicationStatic.IMG_HEIGHT);
	}

	/**
	 * Get Normalize Matrix overload method
	 * 
	 * @param file
	 * @param imgWidth
	 * @param imgHeight
	 * @return
	 */
	public double[][] getNormalizeMatrix(File file, int imgWidth, int imgHeight) {
		try {

			BufferedImage originalImage = ImageIO.read(file);
			int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			BufferedImage resizeImageJpg = resizeImageWithHint(originalImage, type, imgWidth, imgHeight);
			Raster raster = resizeImageJpg.getData();
			int w = raster.getWidth(), h = raster.getHeight();
			double pixels[][] = new double[w][h];
			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					pixels[x][y] = raster.getSample(x, y, 0);
				}
			}
			return pixels;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * resize the Image
	 * 
	 * @param originalImage
	 * @param type
	 * @param imgWidth
	 * @param imgHeight
	 * @return
	 */
	private BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int imgWidth, int imgHeight) {

		BufferedImage resizedImage = new BufferedImage(imgWidth, imgHeight, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, imgWidth, imgHeight, null);
		g.dispose();
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	}
}
